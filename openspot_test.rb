require 'rest-client'
require 'net/http'
require 'json'
require 'digest'

require 'awesome_print'

if ARGV.length != 1
  puts "Usage: ${$0} <openspot ip address>"
  exit 1
end

OSP_ADDR = ARGV[0]
OSP_PASS = ENV.fetch("OSP_PASS", "openspot")

JSON_HEADERS = {
  "Content-Type" => "application/json",
  "Accept" => "application/json",
  "Connection" => "keep-alive"
}

def digest_password(password, token)
  Digest::SHA256.hexdigest("#{token}#{password}")
end

def api_url(path)
  "http://#{OSP_ADDR}/#{path}"
end

def authenticated_json_headers(jwt)
  JSON_HEADERS.merge({
    "Authorization" => "Bearer #{jwt}"
  })
end

puts "Logging in..."
response = RestClient.get api_url("gettok.cgi")
json = JSON.parse(response.body)
token = json.fetch("token")
digest = digest_password(OSP_PASS, token)

response = RestClient.post(api_url("login.cgi"), { token: token, digest: digest }.to_json, JSON_HEADERS)
json = JSON.parse(response.body)

hostname = json.fetch("hostname")
jwt = json.fetch("jwt")

puts "Logged into openspot at #{OSP_ADDR}, hostname: #{hostname}"

STATUS_URI = "/status.cgi"

Net::HTTP.start(OSP_ADDR, 80) do |http|
  current_callsign = ""

  puts "Waiting for calls..."

  loop do
    request = Net::HTTP::Get.new(STATUS_URI)
    request['Connection'] = "keep-alive"
    request['Accept'] = 'application/json'
    request['Authorization'] = "Bearer #{jwt}"

    response = http.request(request)

    json = JSON.parse(response.body.to_s)

    status = json.fetch("status")
    connected_to = json.fetch("connected_to")

    if status == 0
      current_callsign = ""
    elsif status == 1
      callinfo = json.fetch("callinfo")

      if callinfo.length == 1
        current_callsign = callinfo[0][1]
        puts "[#{Time.now}] [#{connected_to}] from: #{current_callsign}"
      end

    end

    sleep 0.5
  end
end

